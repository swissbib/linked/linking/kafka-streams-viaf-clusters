# Kafka Streams: VIAF Clusters

Kafka Streams application consuming [Viaf](https://viaf.org) data as 
N-Triples and converting statements to [JSON-LD](https://json-ld.org/).

The normalization and change in serialization are achieved with a metafacture pipeline. 

All records with type `https://www.wikidata.org/wiki/Q1387388` are filtered, as these are
mythical hybrids. They demarcate old corrupted data, which is not deleted for completeness.

Input is expected to carry the ID as key of the message. This is done by the 
[flink job](https://gitlab.com/swissbib/linked/viaf-grouped-ntriples) to filter, sort and split 
the n-triples dump.

The following changes are made:

- All property namespaces are shortened.
- String literals with language tags which are not EN, DE, FR or IT are removed.
- Datatypes are removed
- clean up birth date and death date
- adds dbo:birthYear and dbo:deathYear.
- replace schema:sameAs with owl:sameAs.
- filters rare and unused triples.
