package org.swissbib.linked;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.swissbib.SbMetadataDeserializer;
import org.swissbib.SbMetadataModel;
import org.swissbib.SbMetadataSerializer;
import org.swissbib.types.EsBulkActions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TestApp {

    private final String morphDefinitionPath = "src/main/resources/mf/";
    private static final KafkaProperties properties = new KafkaProperties();
    private static TopologyTestDriver testDriver;

    @AfterAll
    static void closeDriver() {
        testDriver.close();
    }

    @BeforeAll
    static void createTestDriver() {
        Topology topology = new KafkaTopology(properties.appProperties).build();
        testDriver = new TopologyTestDriver(topology, properties.kafkaProperties);
    }


    private String loadData(String file_name) {
        String path = "src/test/resources/data";
        try {
            return new String(Files.readAllBytes(Paths.get(path, file_name)));
        } catch (IOException ex) {
            return "";
        }
    }

    @Test
    void TestCase1() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        testDriver.pipeInput(
                factory.create(
                        properties.appProperties.getProperty("kafka.topics.source"), "http://viaf.org/viaf/100046",
                        new SbMetadataModel().setData(loadData("input1.nt")).setMessageDate("2019-11-08")
                )
        );
        ProducerRecord<String, SbMetadataModel> output =
                testDriver.readOutput(
                        properties.appProperties.getProperty("kafka.topics.sink"),
                        new StringDeserializer(),
                        new SbMetadataDeserializer()
                );

        assertAll("test case 1: person transformation",
                () -> assertEquals("http://viaf.org/viaf/100046", output.key()),
                () -> assertEquals("viaf-persons-2019-11-08", output.value().getEsIndexName()),
                () -> assertEquals(EsBulkActions.INDEX, output.value().getEsBulkAction()),
                () -> assertEquals(loadData("output1.json"), output.value().getData())
        );
    }

    @Test
    void TestCase2() {
        ConsumerRecordFactory<String, SbMetadataModel> factory = new ConsumerRecordFactory<>(new StringSerializer(), new SbMetadataSerializer());
        testDriver.pipeInput(
                factory.create(
                        properties.appProperties.getProperty("kafka.topics.source"), "http://viaf.org/viaf/6451151051958433530006",
                        new SbMetadataModel().setData(loadData("input2.nt")).setMessageDate("2019-11-08")
                )
        );
        ProducerRecord<String, SbMetadataModel> output =
                testDriver.readOutput(
                        properties.appProperties.getProperty("kafka.topics.sink"),
                        new StringDeserializer(),
                        new SbMetadataDeserializer()
                );

        assertAll("test case 1: creative work transformation",
                () -> assertEquals("http://viaf.org/viaf/6451151051958433530006", output.key()),
                () -> assertEquals("viaf-creative-works-2019-11-08", output.value().getEsIndexName()),
                () -> assertEquals(EsBulkActions.INDEX, output.value().getEsBulkAction()),
                () -> assertEquals(loadData("output2.json"), output.value().getData())
        );
    }

    @Test
    void testDataFlow() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);

        TypedResult output = workflow.process(loadData("input1.nt"), "2019-05-05");

        assertEquals(loadData("output1.json"), output.content.trim());

        TypedResult output2 = workflow.process(loadData("test_data_2.nt"), "2019-05-05");
        assertEquals(loadData("test_data_2.out.json"), output2.content.trim());
    }

    @Test
    void testLanguageTags() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);

        TypedResult output = workflow.process(loadData("test_data_3.nt"), "2019-05-05");

        assertEquals(loadData("test_data_3.out.json"), output.content.trim());
    }

    @Test
    void testDateConversion() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);

        TypedResult output = workflow.process(loadData("test_data_4.nt"), "2019-05-05");

        assertEquals(loadData("test_data_4.out.json"), output.content.trim());
    }

    @Test
    void testTypeFilterEvent() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);
        TypedResult output = workflow.process(loadData("test_type_filter_event.nt"), "2019-05-05");
        assertEquals(Types.EVENT, output.type);
    }

    @Test
    void testTypeFilterCreativeWork() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);
        TypedResult output = workflow.process(loadData("test_type_filter_creative_work.nt"), "2019-05-05");
        assertEquals(Types.CREATIVE_WORK, output.type);
    }

    @Test
    void testTypeFilterPerson() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);
        TypedResult output = workflow.process(loadData("test_type_filter_person.nt"), "2019-05-05");
        assertEquals(Types.PERSON, output.type);
    }

    @Test
    void testTypeFilterOrganization() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);
        TypedResult output = workflow.process(loadData("test_type_filter_organisation.nt"), "2019-05-05");
        assertEquals(Types.ORGANISATION, output.type);
    }

    @Test
    void testTypeFilterPersonOrganization() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);
        TypedResult output = workflow.process(loadData("test_type_filter_person_organisation.nt"), "2019-05-05");
        assertEquals(Types.PERSON, output.type);
    }

    @Test
    void testTypeFilterPlaces() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);
        TypedResult output = workflow.process(loadData("test_type_filter_place.nt"), "2019-05-05");
        assertEquals(Types.PLACE, output.type);
    }

    @Test
    void testTypeFilterPlacesOrganizations() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);
        TypedResult output = workflow.process(loadData("test_type_filter_place_organisation.nt"), "2019-05-05");
        assertEquals(Types.ORGANISATION, output.type);

    }

    @Test
    void testTypeFilterPersonAgent() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);
        TypedResult output = workflow.process(loadData("test_type_filter_person_agent.nt"), "2019-05-05");
        assertEquals(Types.PERSON, output.type);
    }

    @Test
    void testTypeFilterPlaceOrgAgent() {
        MfWorkflow workflow = new MfWorkflow(morphDefinitionPath);
        TypedResult output = workflow.process(loadData("test_type_filter_place_organisation_agent.nt"), "2019-05-05");
        assertEquals(Types.ORGANISATION, output.type);
    }
}
