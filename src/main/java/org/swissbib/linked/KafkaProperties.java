/*
 * viaf-importer
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.swissbib.SbMetadataSerde;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.*;

class KafkaProperties {
    private static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    Properties appProperties;
    Properties kafkaProperties;

    KafkaProperties() {
        List<String> propertiesFilePaths = new ArrayList<>();
        propertiesFilePaths.add("/configs/app.properties");
        this.appProperties = loadAppProperties(propertiesFilePaths);
        this.kafkaProperties = new Properties();
        mapProperties();
    }

    private Properties loadAppProperties(List<String> propertiesFilePaths) {
        Optional<String> path = propertiesFilePaths.stream().filter(p -> {
            File f = new File(p);
            return f.exists() && !f.isDirectory();
        }).findFirst();

        Properties appProperties = new Properties();
        if (path.isPresent()) {
            log.debug("Properties file found on path {}", path.get());
            File f = new File(path.get());
            try {
                appProperties.load(new FileInputStream(f));
            } catch (IOException e) {
                log.error("Can't open file {}", path.get());
                System.exit(1);
            }
        } else {
            log.warn("No properties file in any indicated path found. Loading default properties from class path.");
            try {
                appProperties.load(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream("app.properties")));
            } catch (IOException ex) {
                log.error("Could not load properties file from class path.");
                System.exit(1);
            }
        }
        return appProperties;
    }

    private void mapProperties() {
        setProperty(StreamsConfig.APPLICATION_ID_CONFIG, null, true);
        setProperty("application.server");
        setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, null, true);
        setProperty("buffered.records.per.partition");
        setProperty("cache.max.bytes.buffering");
        setProperty("client.id");
        setProperty("commit.interval.ms");
        setProperty("default.deserialization.exception.handler");
        setProperty("default.production.exception.handler");
        setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName(), true);
        setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SbMetadataSerde.class.getName(), true);
        setProperty("metric.reporters");
        setProperty("metrics.num.samples");
        setProperty("metrics.sample.window.ms");
        setProperty("num.standby.replicas");
        setProperty("num.stream.threads");
        setProperty("partition.grouper");
        setProperty("processing.guarantee");
        setProperty("poll.ms");
        setProperty("replication.factor");
        setProperty("retries");
        setProperty("retry.backoff.ms");
        setProperty("state.cleanup.delay.ms");
        setProperty("state.dir");
        setProperty("timestamp.extractor");
        setProperty("windowstore.changelog.additional.retention.ms");

        setAppProperty("kafka.topics.source", true);
        setAppProperty("kafka.topics.sink", true);
        setAppProperty("es.index.persons", true);
        setAppProperty("es.index.organisations", true);
        setAppProperty("es.index.creativeworks", true);
        setAppProperty("es.index.places", true);
        setAppProperty("es.index.events", true);
        setAppProperty("morph.path", true);
    }

    private void setAppProperty(String propertyName, Boolean abortIfMissing) {
        String envProperty = propertyName.replaceAll("\\.", "_").toUpperCase();
        if (System.getenv(envProperty) != null) {
            log.trace("Found value for property {} in environment variable {}", propertyName, envProperty);
            appProperties.setProperty(propertyName, System.getenv(envProperty));
        } else if (appProperties.getProperty(propertyName) != null) {
            log.trace("Found value for property {} in properties file", propertyName);
        } else if (abortIfMissing) {
            log.error("Required property {} not set! Aborting...", propertyName);
            System.exit(1);
        } else {
            log.trace("No value for property {} found", propertyName);
        }
    }

    private void setProperty(String propertyName, String defaultValue, Boolean abortIfMissing) {
        String envProperty = propertyName.replaceAll("\\.", "_").toUpperCase();
        if (System.getenv(envProperty) != null) {
            log.trace("Found value for property {} in environment variable {}", propertyName, envProperty);
            kafkaProperties.setProperty(propertyName, System.getenv(envProperty));
        } else if (appProperties != null && appProperties.getProperty(propertyName) != null) {
            log.trace("Found value for property {} in properties file", propertyName);
            kafkaProperties.setProperty(propertyName, appProperties.getProperty(propertyName));
        }  else if (defaultValue != null) {
            log.trace("Found default value for property {}: {}", propertyName, defaultValue);
            kafkaProperties.setProperty(propertyName, defaultValue);
        }
        else if (abortIfMissing) {
            log.error("Required property {} not set! Aborting...", propertyName);
            System.exit(1);
        } else {
            log.trace("No value for property {} found", propertyName);
        }
    }

    private void setProperty(String propertyName) {
        setProperty(propertyName, null, false);
    }
}
