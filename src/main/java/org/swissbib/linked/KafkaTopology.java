/*
 * viaf-importer
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked;

import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.swissbib.SbMetadataModel;
import org.swissbib.types.EsBulkActions;

import java.util.Properties;

class KafkaTopology {

    private Properties appProperties;
    KafkaTopology(Properties appProperties) {
        this.appProperties = appProperties;
    }

    Topology build() {
        final StreamsBuilder builder = new StreamsBuilder();

        final String kafkaSink = appProperties.getProperty("kafka.topics.sink");
        final String path = appProperties.getOrDefault("morph.path", "/app/morphs").toString();
        final MfWorkflow mfWorkflow = new MfWorkflow(path);

        KStream<String, SbMetadataModel> source = builder
                .stream(appProperties.getProperty("kafka.topics.source"));
        KStream<String, TypedResult> transformed =
                source.mapValues((key, value) -> mfWorkflow.process(value.getData().trim(), value.getMessageDate()));

        @SuppressWarnings("unchecked") KStream<String, TypedResult>[] branches = transformed.filter((k, v) -> v != null)
                .branch(
                        ((key, value) -> value.type.equals(Types.CREATIVE_WORK)),
                        ((key, value) -> value.type.equals(Types.EVENT)),
                        ((key, value) -> value.type.equals(Types.ORGANISATION)),
                        ((key, value) -> value.type.equals(Types.PERSON)),
                        ((key, value) -> value.type.equals(Types.PLACE))
                );

        branches[0].mapValues((v) -> createSbMetadataModel(v, appProperties.getProperty("es.index.creativeworks")))
                .to(kafkaSink);
        branches[1].mapValues((v) -> createSbMetadataModel(v, appProperties.getProperty("es.index.events")))
                .to(kafkaSink);
        branches[2].mapValues((v) -> createSbMetadataModel(v, appProperties.getProperty("es.index.organisations")))
                .to(kafkaSink);
        branches[3].mapValues((v) -> createSbMetadataModel(v, appProperties.getProperty("es.index.persons")))
                .to(kafkaSink);
        branches[4].mapValues((v) -> createSbMetadataModel(v, appProperties.getProperty("es.index.places")))
                .to(kafkaSink);
        return builder.build();
    }


    private SbMetadataModel createSbMetadataModel(TypedResult t, String indexName) {
        return new SbMetadataModel()
                .setData(t.content.trim())
                .setEsBulkAction(EsBulkActions.INDEX)
                .setEsIndexName(indexName + "-" + t.date);
    }
}
