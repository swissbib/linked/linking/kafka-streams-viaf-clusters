/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked;

import org.metafacture.flowcontrol.ObjectExceptionCatcher;
import org.metafacture.io.ObjectJavaIoWriter;
import org.metafacture.metamorph.Filter;
import org.metafacture.metamorph.Metamorph;
import org.metafacture.plumbing.StreamTee;
import org.metafacture.strings.StringReader;
import org.swissbib.linked.linkeddata.ESBulkEncoder;
import org.swissbib.linked.linkeddata.NtriplesDecoder;

import java.io.Reader;
import java.nio.file.Paths;

class MfWorkflow {
    private StringReader workflow;
    private String path;
    private final WriterFactory creativeWorkWriterBuffer = new WriterFactory();
    private final WriterFactory eventWriterBuffer = new WriterFactory();
    private final WriterFactory organisationWriterBuffer = new WriterFactory();
    private final WriterFactory personWriterBuffer = new WriterFactory();
    private final WriterFactory placeWriterBuffer = new WriterFactory();

    MfWorkflow(String path) {
        this.path = path;
        workflow = createMfWorkflow();
    }

    TypedResult process(String inputString, String indexDate) {
        workflow.process(inputString);
        TypedResult result = readFromWriterBuffer(indexDate);
        workflow.resetStream();
        return result;
    }

    private TypedResult readFromWriterBuffer(String indexDate) {
        TypedResult res = null;
        if (creativeWorkWriterBuffer.writer.getBuffer().length() > 0) {
            res = new TypedResult(Types.CREATIVE_WORK, creativeWorkWriterBuffer.writer.toString(), indexDate);
        }
        if (placeWriterBuffer.writer.getBuffer().length() > 0) {
            res = new TypedResult(Types.PLACE, placeWriterBuffer.writer.toString(), indexDate);
        }
        if (eventWriterBuffer.writer.getBuffer().length() > 0) {
            res = new TypedResult(Types.EVENT, eventWriterBuffer.writer.toString(), indexDate);
        }
        if (organisationWriterBuffer.writer.getBuffer().length() > 0) {
            res = new TypedResult(Types.ORGANISATION, organisationWriterBuffer.writer.toString(), indexDate);
        }
        if (personWriterBuffer.writer.getBuffer().length() > 0) {
            res = new TypedResult(Types.PERSON, personWriterBuffer.writer.toString(), indexDate);
        }
        return res;
    }

    private StringReader createMfWorkflow() {
        final StringReader stringReader = new StringReader();
        final ObjectExceptionCatcher<Reader> objectExceptionCatcher = new ObjectExceptionCatcher<>();
        final NtriplesDecoder ntriplesDecoder = new NtriplesDecoder();
        ntriplesDecoder.setUnicodeEscapeSeq("true");
        ntriplesDecoder.setEachLineIsRecord("false");
        final StreamTee streamTee = new StreamTee();
        final Filter creativeWorkFilter = new Filter(Paths.get(path,"creativeWorkFilter.xml").toString());
        final Filter eventFilter = new Filter(Paths.get(path,"eventFilter.xml").toString());
        final Filter organisationFilter = new Filter(Paths.get(path,"organisationFilter.xml").toString());
        final Filter personFilter = new Filter(Paths.get(path,"personFilter.xml").toString());
        final Filter placeFilter = new Filter(Paths.get(path,"placeFilter.xml").toString());
        streamTee.addReceiver(creativeWorkFilter)
                .addReceiver(eventFilter)
                .addReceiver(organisationFilter)
                .addReceiver(personFilter)
                .addReceiver(placeFilter);
        stringReader
                .setReceiver(objectExceptionCatcher)
                .setReceiver(ntriplesDecoder)
                .setReceiver(streamTee);
        appendWorkflow(personFilter, Paths.get(path,"morphPerson.xml").toString(), personWriterBuffer);
        appendWorkflow(organisationFilter, Paths.get(path,"morphOrganisation.xml").toString(), organisationWriterBuffer);
        appendWorkflow(placeFilter, Paths.get(path,"morph.xml").toString(), placeWriterBuffer);
        appendWorkflow(eventFilter, Paths.get(path,"morphEvents.xml").toString(), eventWriterBuffer);
        appendWorkflow(creativeWorkFilter, Paths.get(path,"morph.xml").toString(), creativeWorkWriterBuffer);
        return stringReader;
    }

    private static void appendWorkflow(Filter filter, String pathToMorphFile, WriterFactory buffer) {
        final Metamorph morph = new Metamorph(pathToMorphFile);
        final ESBulkEncoder esBulkEncoder = new ESBulkEncoder();
        esBulkEncoder.setEscapeChars("true");
        esBulkEncoder.setAvoidMergers("false");
        esBulkEncoder.setHeader("false");
        esBulkEncoder.setIndex("");
        final ObjectJavaIoWriter<String> writer = new ObjectJavaIoWriter<>(buffer);
        filter
                .setReceiver(morph)
                .setReceiver(esBulkEncoder)
                .setReceiver(writer);
    }
}